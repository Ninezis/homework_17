#include <iostream>
#include <math.h>

using namespace std;

class Vector
{
public:
	Vector()
	{}

	void Show() 
	{
		cout << "The vector is: " << '(' << x << "; " << y << "; " << z << ')' << endl;
	}
	
	void ModulOfVector()
	{
		a = sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
		cout << "The module of the vector is: " << a << endl;
	}

private:
	double a;
	double x = 12.3;
	double y = 4.34;
	double z = 1;

};


int main()
{
	Vector v;
	v.Show();
	cout << endl;
	v.ModulOfVector();

	return 0;
}